# Getting Started with AsyncIO for Python
This is project to get familiarize with AsyncIO library for Python program. This library enable a concurrent tasks to be run, communicate between tasks using queues.  

Similar to Real-Time OS, this library use task scheduling to manage tasks concurrently and it use co-operative task scheduling algorithm. This library claimed to be more  
efficient for small code space compared to using thread in Python.

# Keyword and Explanation

| Keyword | Description                                                                             |
|---------|-----------------------------------------------------------------------------------------|
| await   | Every function or statement that required return product shall use Await keyword prefix |
| async   | Every function shall use Async keyword prefix                                           |
| future  | A Future represents an eventual result of an asynchronous operation. Not thread-safe    |